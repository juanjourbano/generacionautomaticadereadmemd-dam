#!/bin/bash

# Modo de uso:
# Mediante un terminal hay que posicionarse en el directorio del proyecto actual.
# Ejemplo: cd /home/vesprada/IdeaProjects/prog-ud8-classwork

# Una vez dentro hay que ejecutar el script (la ruta dependerá de dónde lo tengas ubicado).
# Por ejemplo si lo tienes en tu escritorio puedes que ejecutarlo así mediante ruta absoluta: /home/vesprada/Escritorio/generarReadme.sh
# O mediante ruta relativa: ../../Escritorio/generarReadme.sh

# El script interpreta que si el proyecto tiene un directorio resources lo que contenga será tratado como diagramas(imágenes) y añadidos al README.md
# Después se añaden las actividades, habrá que indicar la ruta donde estén los paquetes principales.
# Se añadirán también enlaces a las clases, interpretadas como el contenido de cada paquete (actividad).
# La descripción de cada actividad se recogerá también automáticamente, el script buscará en todas las clases del paquete y 
# si en alguna encuentra un comentario de bloque será interpretado como la descripción de la actividad, se sustraerá y añadirá al README.md

# Una vez termine se generará el archivo README.md dentro de tu proyecto.

dirActual=$PWD

nombreProyecto=$(basename $dirActual)

echo Introduce la ruta donde se encuentran los paquetes principales que contienen las clases
echo Ejemplo: /es/jurbano
echo Si los paquetes principales se encuentran en src omite esta parte pulsando intro

read rutaPaquetes

rutaAbsSRC=$dirActual/src$rutaPaquetes
rutaRelSRC=src$rutaPaquetes

echo "## ${nombreProyecto^^} - Actividades" >> $dirActual/README.md
echo "" >> $dirActual/README.md

if [ -d $dirActual/resources ]
then
	echo "<h2>DIAGRAMAS</h2>" >> $dirActual/README.md	
	ls $dirActual/resources/ | sort -V >> $dirActual/diagramasOrdenados.txt
	echo "[**Resources:**](resources/)" >> $dirActual/README.md

	cat diagramasOrdenados.txt | while read diagrama || [[ -n $diagrama ]]; do

		echo ">>>" >> $dirActual/README.md
		echo "**${diagrama^}:**" >> $dirActual/README.md
		echo '<p align="center">!['${diagrama}'](resources/'${diagrama}')</p>' >> $dirActual/README.md
		echo ">>>" >> $dirActual/README.md		

	done

fi

rm $dirActual/diagramasOrdenados.txt

ls $rutaAbsSRC/ | sort -V >> $dirActual/directoriosOrdenados.txt

echo "<h2>ACTIVIDADES</h2>" >> $dirActual/README.md

cat $dirActual/directoriosOrdenados.txt | while read linea || [[ -n $linea ]]; do

	if [ -d $rutaAbsSRC/$linea ]
	then
		nombreDirectorio=$(basename $linea)
		echo "[**${nombreDirectorio^}:**]($rutaRelSRC/$linea)" >> $dirActual/README.md

		echo ">>>" >> $dirActual/README.md
		for f in $rutaAbsSRC/$linea/*; do

			sed -n "/\/\*/=" $f
			rango1=$(sed -n "/\/\*/=" $f)
			rango2=$(sed -n "/\*\//=" $f)

			sed -n "$rango1,$rango2 p" $f | sed "s=/==g" | sed "s/*//g" >> $dirActual/README.md

		done
		echo ">>>" >> $dirActual/README.md

		echo ">>>" >> $dirActual/README.md
		echo "Contenido:" >> $dirActual/README.md
		for f in $rutaAbsSRC/$linea/*; do
			
			echo "| [**$(basename $f .java)**]($rutaRelSRC/$linea/$(basename $f)) |" >> $dirActual/README.md

		done
		echo ">>>" >> $dirActual/README.md
		
	fi

done

rm $dirActual/directoriosOrdenados.txt